export interface HomeInterface {
    id?: number,
    name: string,
    lastName: string
}