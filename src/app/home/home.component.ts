import { AfterViewInit, Component, ViewChild, OnInit } from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatTableDataSource} from '@angular/material/table';
import {HomeService} from '../service/home.service'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})

export class HomeComponent implements OnInit{
  loading: boolean
  dataSource = []

  constructor(
    private home: HomeService,
  ) { }

  ngOnInit() {

    this.loading = true
    
    this.getDatas();
  }

  getDatas(){
    this.loading = true
    this.home.getDatas().subscribe(data => {
      this.loading = false
      this.dataSource = data
    })
  }

  remove(id){
    this.loading = true
    this.home.delete(id).subscribe(data => {
      console.log(data)
      this.loading = false
      this.dataSource = data

      this.getDatas()
    })
  }

  edit(){
    alert("d")
  }

  reload(){
    this.loading = true
    this.home.getDatas().subscribe(data => {
      this.loading = false
      this.dataSource = data
    })
  }
}
