import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams} from '@angular/common/http'
import { Observable } from 'rxjs/internal/Observable'
import { environment } from '../../environments/environment';
import { HomeInterface } from '../models/home'

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  env = environment;

  constructor(
    private http: HttpClient
  ) { }

  headers : HttpHeaders = new HttpHeaders({
    
  });

  getDatas(): Observable<any>{
    const url = this.env.rootUrl+'api/users/getUsers'
    return this.http.get(url,{headers:this.headers})
  }

  getById(id): Observable<any>{
    const url = this.env.rootUrl+'api/users/getByIdUsers?id=' + id
    return this.http.get(url,{headers:this.headers})
  }

  delete(id): Observable<any>{
    const url = this.env.rootUrl+'api/users/deleteUsers?id=' + id
    return this.http.post(url,{headers:this.headers})
  }

  put(id, u:HomeInterface): Observable<any>{
    var parameter = 
      {
        nombre:u.name,
        apellido:u.lastName
      };
    const url = this.env.rootUrl+'api/users/putUsers?id=' + id
    return this.http.post(url,parameter, {headers:this.headers})
  }

  post(u:HomeInterface): Observable<any>{
    var parameter = 
      {
        nombre:u.name,
        apellido:u.lastName
      };

    const url = this.env.rootUrl+'api/users/postUsers'
    return this.http.post(url,parameter, {headers:this.headers})
  }
}
